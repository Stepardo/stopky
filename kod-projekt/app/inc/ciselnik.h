#ifndef STM8_TOOLS_CISELNIK_H
#define STM8_TOOLS_CISELNIK_H
#include "stm8s_it.h"

void init_sec_units(void);
void show_sec_units(uint8_t number);

void init_sec_dozents(void);
void show_sec_dozents(uint8_t number);

void init_min_units(void);
void show_min_units(uint8_t number);

void init_min_dozents(void);
void show_min_dozents(uint8_t number);
#endif //STM8_TOOLS_CISELNIK_H
