#ifndef INT_DELAY_H
#define INC_DELAY_H

#include "stm8s_tim4.h"
void delay_init(void);
void delay_ms(uint16_t time_ms);

#endif