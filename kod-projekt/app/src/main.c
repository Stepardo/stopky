#include "stm8s.h"
#include "delay.h"
#include "ciselnik.h"

int stop = 0;
int first = 1;
int reset = 0;
int start = 0;
int has_started = 0;

INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
{
    if (first)
    {
        stop = 1;
        first = 0;
    }
    else
    {
        stop = 0;
        first = 1;
    }
}

INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
    if (!has_started)
    {
        start = 1;
    }
    if (first && has_started)
    {
        reset = 1;
        first = 0;
    }
    else
    {
        reset = 0;
        first = 1;
    }
    has_started = 1;
}

void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);
    GPIO_Init(GPIOE, GPIO_PIN_5, GPIO_MODE_IN_FL_IT);
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_ONLY);
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOC,EXTI_SENSITIVITY_RISE_ONLY);
    enableInterrupts();

    delay_init();
    init_sec_units();
    init_sec_dozents();
    init_min_units();
    init_min_dozents();

    show_min_dozents(0);
    show_min_units(0);
    show_sec_dozents(0);
    show_sec_units(0);

    while (1) {
        if(start) {
            for (int i = 0; i < 6 && !(reset); i++) {
                while (stop) {}
                show_min_dozents(i);
                for (int j = 0; j < 10 && !(reset); j++) {
                    while (stop) {}
                    show_min_units(j);
                    for (int k = 0; k < 6 && !(reset); k++) {
                        while (stop) {}
                        show_sec_dozents(k);
                        for (int l = 0; l < 10 && !(reset); l++) {
                            while (stop) {}
                            show_sec_units(l);
                            delay_ms(1000);

                        }

                    }

                }

            }
        }
    }
}
