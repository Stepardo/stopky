# include "delay.h"


void delay_init(void)
{
    TIM4_TimeBaseInit(TIM4_PRESCALER_64, 249);
    TIM4_Cmd(ENABLE);
}

void delay_ms(uint16_t time_ms)
{
    TIM4_SetCounter(0);
    for(int ms = 0; ms < time_ms; ms++)
    {
        while(TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET);
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
    }
}