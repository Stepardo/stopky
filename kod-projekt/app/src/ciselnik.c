#include "ciselnik.h"

void init_sec_units()
{
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW); // 1
    GPIO_Init(GPIOG, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); // 2
    GPIO_Init(GPIOG, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW); // 3
    GPIO_Init(GPIOG, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW); // 4
}
void show_sec_units(uint8_t number)
{
    if(number == 1)
    {
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 2)
    {
        GPIO_WriteLow(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 3)
    {
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 4)
    {
        GPIO_WriteLow(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 5)
    {
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 6)
    {
        GPIO_WriteLow(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 7)
    {
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
    else if(number == 8)
    {
        GPIO_WriteLow(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
    }
    else if(number == 9)
    {
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
    }
    else if(number == 0)
    {
        GPIO_WriteLow(GPIOG, GPIO_PIN_4);
        GPIO_WriteLow(GPIOG, GPIO_PIN_5);
        GPIO_WriteLow(GPIOG, GPIO_PIN_6);
        GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    }
}

void init_sec_dozents()
{
    GPIO_Init(GPIOF, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW); // 1
    GPIO_Init(GPIOF, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); // 2
    GPIO_Init(GPIOF, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW); // 3
    GPIO_Init(GPIOF, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW); // 4
}
void show_sec_dozents(uint8_t number)
{
    if(number == 1)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 2)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 3)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 4)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 5)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 6)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 7)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
    else if(number == 8)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_7);
    }
    else if(number == 9)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOF, GPIO_PIN_7);
    }
    else if(number == 0)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_4);
        GPIO_WriteLow(GPIOF, GPIO_PIN_5);
        GPIO_WriteLow(GPIOF, GPIO_PIN_6);
        GPIO_WriteLow(GPIOF, GPIO_PIN_7);
    }
}

void init_min_units()
{
    GPIO_Init(GPIOF, GPIO_PIN_0, GPIO_MODE_OUT_PP_HIGH_SLOW); // 1
    GPIO_Init(GPIOD, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW); // 2
    GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_PP_HIGH_SLOW); // 3
    GPIO_Init(GPIOG, GPIO_PIN_1, GPIO_MODE_OUT_PP_HIGH_SLOW); // 4
}
void show_min_units(uint8_t number)
{
    if(number == 1)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 2)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 3)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 4)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 5)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 6)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 7)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
    else if(number == 8)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_1);
    }
    else if(number == 9)
    {
        GPIO_WriteHigh(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_1);
    }
    else if(number == 0)
    {
        GPIO_WriteLow(GPIOF, GPIO_PIN_0);
        GPIO_WriteLow(GPIOD, GPIO_PIN_2);
        GPIO_WriteLow(GPIOD, GPIO_PIN_0);
        GPIO_WriteLow(GPIOG, GPIO_PIN_1);
    }
}

void init_min_dozents()
{
    GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_HIGH_SLOW); // 1
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_HIGH_SLOW); // 2
    GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_OUT_PP_HIGH_SLOW); // 3
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_HIGH_SLOW); // 4
}

void show_min_dozents(uint8_t number)
{
    if(number == 1)
    {
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 2)
    {
        GPIO_WriteLow(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 3)
    {
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 4)
    {
        GPIO_WriteLow(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 5)
    {
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 6)
    {
        GPIO_WriteLow(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 7)
    {
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
    else if(number == 8)
    {
        GPIO_WriteLow(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if(number == 9)
    {
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if(number == 0)
    {
        GPIO_WriteLow(GPIOB, GPIO_PIN_2);
        GPIO_WriteLow(GPIOB, GPIO_PIN_3);
        GPIO_WriteLow(GPIOB, GPIO_PIN_4);
        GPIO_WriteLow(GPIOB, GPIO_PIN_5);
    }
}