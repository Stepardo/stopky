# Stopky



## Obsah
    Stopky řízené STM8 Nucleo, maximální hodnota počítání 59:59. (59 minut a 59 sekund)
    Na výstupu jsou čtyři 7segmentové displeje ovladané přes BCD dekodéry.

## Blokové schéma
```mermaid
flowchart LR
    PC-->STM8
subgraph STM8 Nucleo
    STM8-->G4-7
    STM8-->F4-7
    STM8-->F0,D2,D0,G1
    STM8-->B2-5
    STM8-->C4
    STM8-->E5
    end
    C4-->STOP-BTN
    E5-->RESET-BTN
    G4-7-->1.Dekoder
    F4-7-->2.Dekoder
    F0,D2,D0,G1-->3.Dekoder
    B2-5-->4.Dekoder
    1.Dekoder-->1.Sedmisegment
    2.Dekoder-->2.Sedmisegment
    3.Dekoder-->3.Sedmisegment
    4.Dekoder-->4.Sedmisegment
```
## Seznam součástek
| Typ součastky | Hodnota | Počet kusů |  Cena za kus |  Cena dohromady |
|:-------------:|:-------:|:----------:|:------------:|:---------------:|
| Dekodér D147D |         |      4     |     24Kč     |       96Kč      |
| 7seg. displej |         |      4     |      7Kč     |       28Kč      |
|    Rezistor   |   330R  |     29     |      2Kč     |       58Kč      |
|     Káblíky   |         |     50     |      1Kč     |       50Kč      |
| STM8 Nucleo   |         |      1     |    250Kč     |      250Kč      |
| Celkem:       |         |            |              |      482Kč      |

## Zapojení
![Zapojení na nepájívém poli](zapojeni.jpg)

## Kicad schéma
![Schémá zapojení v KiCadu](schema_kicad.png)

## Zdrojový kód
    Zdrojový kód je součástí přílohy.

## Závěr
    Při dělání projektu jsem narazil na pár obtíží, například jak správně naklonovat repozitář z Gitlabu pana učitele nebo špatného zapojení dekodéru.
    Jinak si myslím že na svou úroveň jsem projekt zvládl dobře.

## Autor
  Štěpán Schäfer